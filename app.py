import os
import connexion
import config
#from models import db
from flask import Flask, current_app


def create_app(config_name):

    '''
    Basic factory to set up the application.

    Parameters
    ----------
    config_name : str
        Name of the configuration object as defined in conig.py

    Returns
    -------
    app 
        The Flask application object
    '''

    config_object = ".".join(('config',config_name))
    connexion_app = connexion.FlaskApp(__name__, port=9090, specification_dir='swagger/')
    
    connexion_app.add_api('indexer.yaml', arguments={'title': 'Vimcar API'})

    flask_app = connexion_app.app
    
    flask_app.config.from_object(config_object)
    
    return connexion_app, flask_app


if __name__ == '__main__':
    connexion_app,flask_app = create_app(os.environ['ENV_SETTINGS'])
    #db.init_app(flask_app)
    #with flask_app.app_context():
        #print(dir(current_app))
    connexion_app.run()