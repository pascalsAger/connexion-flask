class BaseConfig(object):
    """Base configuration."""
    SECRET_KEY = 'thisissecret'
    DEBUG = False



class DevelopmentConfig(BaseConfig):
    """Development configuration."""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:vimcar@127.0.0.1:5432/vimcar'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'thisisdevsecret'