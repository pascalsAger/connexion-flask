from flask import request, url_for
from itsdangerous import URLSafeTimedSerializer
ts = URLSafeTimedSerializer('SECRET_KEY')
from werkzeug.security import generate_password_hash, check_password_hash

def user_signup():
    email = request.headers.get('email')
    password = request.headers.get('password')
    print(email)
    print(password)
    #hashed_password = generate_password_hash(password, method='sha256')
    #new_user = User(email=email, password=hashed_password)
    #db.session.add(new_user)
    #db.session.commit()
    token = ts.dumps(email, salt="email-confirm-key")

    confirm_url = url_for(
        '/v1_0.apis_confirm_user_confirm',
        token=token,
        _external=True)
    return confirm_url


'''from models import db
from models.users import Users'''